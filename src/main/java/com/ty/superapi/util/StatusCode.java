package com.ty.superapi.util;

/**
 * 响应状态码信息
 *
 * @author tianyu
 * @since 2017/11/15
 */
public enum StatusCode {
    _200(200, "请求成功"),
    _400(400, "Bad Request！"),
    _401(401, "NotAuthorization！"),
    _403(403, "Forbidden！"),
    _405(405, "Method Not Allowed！"),
    _406(406, "Not Acceptable！"),
    _500(500, "Internal Server Error！"),
    _1000(1000, "[服务器]运行时异常"),
    _1001(1001, "[服务器]空指针异常"),
    _1002(1002, "[服务器]数据类型转换异常"),
    _1003(1003, "[服务器]IO异常"),
    _1004(1004, "[服务器]未知方法异常"),
    _1005(1005, "[服务器]数组越界异常"),
    _1006(1006, "[服务器]网络异常"),

    _1010(1010, "帐号未注册"),
    _1011(1011, "帐号已注册"),
    _1012(1012, "密码错误"),
    _1013(1013, "帐号被冻结"),
    _1014(1014, "帐号或密码错误"),
    _1019(1019, "未知错误"),

    _1020(1020, "验证码发送失败"),
    _1021(1021, "验证码失效"),
    _1022(1022, "验证码错误"),
    _1023(1023, "短信平台异常"),

    _2010(2010, "未登录或登录时间过长"),
    _2011(2011, "无访问权限"),
    _2012(2012, "无效的Token"),
    _2013(2013, "缺少参数或参数不合法");

    private Integer code;
    private String msg;

    private StatusCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    // 普通方法
    public String getMsg(Integer code) {
        for (StatusCode c : StatusCode.values()) {
            if (c.getCode() == code) {
                return c.msg;
            }
        }
        return null;
    }

    // get set
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
