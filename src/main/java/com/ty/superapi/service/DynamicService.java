package com.ty.superapi.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ty.superapi.dao.DynamicDao;
import com.ty.superapi.entity.Dynamic;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tianyu
 * @since 2017-12-22
 */
@Service
public class DynamicService extends ServiceImpl<DynamicDao, Dynamic> {

}
