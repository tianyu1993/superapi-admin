package com.ty.superapi.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ty.superapi.dao.PositionDao;
import com.ty.superapi.entity.Position;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@Service
public class PositionService extends ServiceImpl<PositionDao, Position> {

}
