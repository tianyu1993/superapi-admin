package com.ty.superapi.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tianyu
 * @since 2017-12-22
 */
public class Project extends Model<Project> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 项目名
     */
	private String name;
    /**
     * 描述
     */
	private String description;
    /**
     * 创建日期
     */
	@TableField("create_date")
	private Date createDate;
    /**
     * 修改日期
     */
	@TableField("update_date")
	private Date updateDate;
    /**
     * 版本
     */
	private String version;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Project{" +
			"id=" + id +
			", name=" + name +
			", description=" + description +
			", createDate=" + createDate +
			", updateDate=" + updateDate +
			", version=" + version +
			"}";
	}
}
