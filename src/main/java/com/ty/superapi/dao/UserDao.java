package com.ty.superapi.dao;

import com.ty.superapi.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
public interface UserDao extends BaseMapper<User> {

}