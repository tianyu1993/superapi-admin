package com.ty.superapi.dao;

import com.ty.superapi.entity.Project;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
public interface ProjectDao extends BaseMapper<Project> {

    public List<Map<String, Object>> selectByUserId(Integer id);

    public Integer insertProject(Project project);

    public List<Integer> selectProMember(Integer id);

}